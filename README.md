# tetris

Javascript Tetris game forked from [here](https://github.com/jakesgordon/javascript-tetris) for publishing on LBRY.

# Deployment on LBRY

Install [lpack-bash](https://gitlab.com/funapplic66/lpack-bash) then run:

```
lpack deploy output/tetris-xyz.lbry
```

Then upload the resulting .lbry file to LBRY.